namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class album_capcity_delete : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ClientAlbums", "AlbumCapacity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ClientAlbums", "AlbumCapacity", c => c.Int(nullable: false));
        }
    }
}
