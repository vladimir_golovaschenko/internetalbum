namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.AlbumImages", new[] { "clientAlbum_Id" });
            DropIndex("dbo.ClientProfiles", new[] { "clientAlbum_Id" });
            CreateIndex("dbo.AlbumImages", "ClientAlbum_Id");
            CreateIndex("dbo.ClientProfiles", "ClientAlbum_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ClientProfiles", new[] { "ClientAlbum_Id" });
            DropIndex("dbo.AlbumImages", new[] { "ClientAlbum_Id" });
            CreateIndex("dbo.ClientProfiles", "clientAlbum_Id");
            CreateIndex("dbo.AlbumImages", "clientAlbum_Id");
        }
    }
}
