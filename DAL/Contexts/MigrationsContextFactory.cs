﻿using System.Data.Entity.Infrastructure;

namespace DAL.Contexts
{
    public class MigrationsContextFactory : IDbContextFactory<ApplicationContext>
    {
        public ApplicationContext Create()
        {
            return new ApplicationContext("DefaultConnection");
        }
    }
}
