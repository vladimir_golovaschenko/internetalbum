﻿using System.Data.Entity;
using DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Contexts
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext(string connectionString)
            : base(connectionString)
        {
        }
        public DbSet<ClientProfile> ClientProfiles { get; set; }
        public DbSet<ClientAlbum> ClientAlbums { get; set; }
        public DbSet<AlbumImage> AlbumImages { get; set; }
    }
}
