﻿using System;
using System.Threading.Tasks;
using DAL.Identity;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext db;

        private readonly ApplicationUserManager userManager;
        private readonly ApplicationRoleManager roleManager;
        private readonly AlbumRepository albumRepository;
        private readonly ImageRepository imageRepository;
        private readonly IClientManager clientManager;

        public UnitOfWork(string connectionString)
        {
            db = new ApplicationContext(connectionString);
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
            clientManager = new ClientManager(db);
            albumRepository = new AlbumRepository(db);
            imageRepository = new ImageRepository(db);
        }

        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

        public IClientManager ClientManager
        {
            get { return clientManager; }
        }


        public ApplicationRoleManager RoleManager
        {
            get { return roleManager; }
        }

        public IRepository<ClientAlbum> ClientAlbums
        {
            get { return albumRepository; }
        }

        public IRepository<AlbumImage> AlbumeImages
        {
            get { return imageRepository; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                    roleManager.Dispose();
                    clientManager.Dispose();
                    imageRepository.Dispose();
                    albumRepository.Dispose();
                    db.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}
