﻿using System.Collections.Generic;
using DAL.Entities;
using DAL.Contexts;
using DAL.Interfaces;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ClientManager : IClientManager
    {
        public ApplicationContext Database { get; set; }
        public ClientManager(ApplicationContext db)
        {
            Database = db;
        }
        public void Create(ClientProfile clientProfile)
        {
            Database.ClientProfiles.Add(clientProfile);
        }

        public ClientProfile Get(string id)
        {
            return Database.ClientProfiles.Find(id);
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<ClientProfile> GetAllUsers()
        {
            return Database.ClientProfiles;
        }
    }
}
