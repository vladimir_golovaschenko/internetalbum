﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Interfaces;
using System.Data.Entity;
using DAL.Entities;
using DAL.Contexts;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class AlbumRepository : IDisposable, IRepository<ClientAlbum>
    {
        private readonly ApplicationContext db;

        public AlbumRepository(ApplicationContext context)
        {
            db = context;
        }
        public void Create(ClientAlbum album)
        {
            db.ClientAlbums.Add(album);
        }

        public void Delete(int id)
        {
            ClientAlbum clientAlbum = db.ClientAlbums.Find(id);

            if (clientAlbum != null)
            {
                db.ClientAlbums.Remove(clientAlbum);
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public IEnumerable<ClientAlbum> Find(Func<ClientAlbum, bool> predicate)
        {
            return db.ClientAlbums.Where(predicate).ToList();
        }

        public ClientAlbum Get(int id)
        {
            return db.ClientAlbums.Find(id);
        }

        public IEnumerable<ClientAlbum> GetAll()
        {
            return db.ClientAlbums;
        }

        public IEnumerable<ClientAlbum> GetAllById(int id)
        {
            return db.ClientAlbums;
        }

        public void Update(ClientAlbum album)
        {
            db.Entry(album).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
