﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Contexts;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ImageRepository : IDisposable, IRepository<AlbumImage>
    {
        private readonly ApplicationContext db;
        public ImageRepository(ApplicationContext context)
        {
            db = context;
        }
        public void Create(AlbumImage image)
        {
            db.AlbumImages.Add(image);
        }

        public void Delete(int id)
        {
            AlbumImage albumImage = db.AlbumImages.Find(id);
            
            if (albumImage != null)
            {
                db.AlbumImages.Remove(albumImage);
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public IEnumerable<AlbumImage> Find(Func<AlbumImage, bool> predicate)
        {
            return db.AlbumImages.Include(i => i.ClientAlbum).Where(predicate).ToList();
        }

        public AlbumImage Get(int id)
        {
            return db.AlbumImages.Find(id);
        }

        public IEnumerable<AlbumImage> GetAllById(int id)
        {
            return db.AlbumImages.Where(i => i.ClientAlbum.Id == id).Include(i => i.ClientAlbum);
        }

        public IEnumerable<AlbumImage> GetAll()
        {
            return db.AlbumImages;
        }

        public void Update(AlbumImage image)
        {
            db.Entry(image).State = EntityState.Modified;
        }
    }
}
