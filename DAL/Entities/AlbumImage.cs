﻿using System;

namespace DAL.Entities
{
    public class AlbumImage
    {
        public int Id { get; set; }
        public virtual ClientAlbum ClientAlbum { get; set; }
        public byte[] Image { get; set; }
        public DateTime UploadDate { get; set; }
    }
}
