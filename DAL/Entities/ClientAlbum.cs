﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class ClientAlbum
    {
        public int Id { get; set; }
        public ICollection<AlbumImage> AlbumImages { get; set; }
        public DateTime LastModified { get; set; }
    }
}
