﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IClientManager : IDisposable
    {
        void Create(ClientProfile clientProfile);
        ClientProfile Get(string id);
        IEnumerable<ClientProfile> GetAllUsers();
    }
}
