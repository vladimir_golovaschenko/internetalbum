﻿using System;
using System.Threading.Tasks;
using DAL.Identity;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IUnitOfWork: IDisposable
    {
        ApplicationUserManager UserManager { get; }
        IClientManager ClientManager { get; }
        IRepository<ClientAlbum> ClientAlbums { get; }
        IRepository<AlbumImage> AlbumeImages { get; }
        ApplicationRoleManager RoleManager { get; }
        Task SaveAsync();
    }
}
