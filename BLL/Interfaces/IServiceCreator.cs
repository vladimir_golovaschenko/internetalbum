﻿namespace BLL.Interfaces
{
    public interface IServiceCreator
    {
        IUserService CreateUserService(string connection);
        IAlbumService CreateAlbumService(string connection);
        IImageService CreateImageService(string connection);
    }
}
