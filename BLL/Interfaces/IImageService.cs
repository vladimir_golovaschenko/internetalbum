﻿using System;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Infrastructure;

namespace BLL.Interfaces
{
    public interface IImageService : IDisposable
    {
        Task<OperationDetails> Create(ImageDTO imageDTO);
        OperationDetails Delete(ImageDTO imageDTO);
    }
}
