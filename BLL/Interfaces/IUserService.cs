﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;
using BLL.DTO;
using BLL.Infrastructure;

namespace BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<OperationDetails> Create(UserDTO userDto);
        UserDTO GetUser(string id);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task SetInitialData(UserDTO adminDto, List<string> roles);
        IEnumerable<UserDTO> GetUsers();
    }
}
