﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Infrastructure;

namespace BLL.Interfaces
{
    public interface IAlbumService : IDisposable
    {
        Task<OperationDetails> CreateAlbum(AlbumDTO albumDTO);
        ImageDTO GetImage(int Id);
        Task <IEnumerable<ImageDTO>> GetImages(int id);
        IEnumerable<ImageDTO> GetAllImages();
        Task <OperationDetails> UpdateAlbum(AlbumDTO albumDTO);
    }
}
