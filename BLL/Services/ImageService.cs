﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Entities;
using AutoMapper;

namespace BLL.Services
{
    public class ImageService : IImageService
    {
        IUnitOfWork Database { get; set; }

        public ImageService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }
        public async Task<OperationDetails> Create(ImageDTO imageDTO)
        {
            AlbumImage albumImage = Database.AlbumeImages.Get(imageDTO.Id);
            if (albumImage == null)
            {
                albumImage = new AlbumImage { Image = imageDTO.Image, ClientAlbum = Database.ClientAlbums.Get(imageDTO.AlbumId), UploadDate = imageDTO.UploadDate };
                Database.AlbumeImages.Create(albumImage);
                await Database.SaveAsync();
                return new OperationDetails(true, "Image created", $"{albumImage.Id}");
            }
            else
            {
                return new OperationDetails(false, "Image with this Id already exists", "");
            }
        }

        public OperationDetails Delete(ImageDTO imageDTO)
        {
            AlbumImage albumImage = Database.AlbumeImages.Get(imageDTO.Id);
            if (albumImage != null)
            {
                Database.AlbumeImages.Delete(albumImage.Id);
                OperationDetails ops = new OperationDetails(true, "Successfult deleted", "");
                return ops;
            }
            else
            {
                return new OperationDetails(false, "Image with this Id does not exists", "");
            }
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
