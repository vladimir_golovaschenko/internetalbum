﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interfaces;
using DAL.Repositories;

namespace BLL.Services
{
    public class ServiceCreator : IServiceCreator
    {
        public IAlbumService CreateAlbumService(string connection)
        {
            return new AlbumService(new UnitOfWork(connection));
        }

        public IImageService CreateImageService(string connection)
        {
            return new ImageService(new UnitOfWork(connection));
        }

        public IUserService CreateUserService(string connection)
        {
            return new UserService(new UnitOfWork(connection));
        }
    }
}
