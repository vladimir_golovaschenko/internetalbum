﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Entities;
using AutoMapper;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }
        IMapper Mapper { get; set; }
        public UserService(IUnitOfWork uow)
        {
            Database = uow;
            Mapper = new MapperConfiguration(cnf => cnf.CreateMap<ClientProfile, UserDTO>()).CreateMapper();
        }
        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            ApplicationUser applicationUser = await Database.UserManager.FindByEmailAsync(userDto.Email);

            if (applicationUser == null)
            {
                applicationUser = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
                var result = await Database.UserManager.CreateAsync(applicationUser, userDto.Password);

                if (result.Errors.Count() > 0)
                {
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                }

                await Database.UserManager.AddToRoleAsync(applicationUser.Id, userDto.Role);

                ClientProfile clientProfile = new ClientProfile { 
                    Id = applicationUser.Id, 
                    Name = userDto.Name, 
                    Email = userDto.Email, 
                    ClientAlbum = Database.ClientAlbums.Get(userDto.AlbumId), 
                    RegistrationDate = userDto.RegistrationDate
                };

                Database.ClientManager.Create(clientProfile);

                return new OperationDetails(true, "Successfully registered.", "");
            }
            else
            {
                return new OperationDetails(false, "User with this Email already exists.", "");
            }
        }

        public UserDTO GetUser(string id)
        {
            ClientProfile clientProfile = Database.ClientManager.Get(id);

            if (clientProfile != null)
            {
                UserDTO user = new UserDTO
                {
                    Id = clientProfile.Id,
                    AlbumId = clientProfile.ClientAlbum.Id,
                    Email = clientProfile.Email,
                    Name = clientProfile.Name,
                    RegistrationDate = clientProfile.RegistrationDate
                };
                return user;
            }

            return null;
        }
        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claimsIdentity = null;
            ApplicationUser applicationUser = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);

            if (applicationUser != null)
            {
                claimsIdentity = await Database.UserManager.CreateIdentityAsync(applicationUser, DefaultAuthenticationTypes.ApplicationCookie);
            }

            return claimsIdentity;
        }
       
        public async Task SetInitialData(UserDTO adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await Database.RoleManager.FindByNameAsync(roleName);

                if (role == null)
                {
                    role = new ApplicationRole { Name = roleName };
                    await Database.RoleManager.CreateAsync(role);
                }
            }

            await Create(adminDto);
        }
        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            return Mapper.Map<IEnumerable<ClientProfile>, List<UserDTO>>(Database.ClientManager.GetAllUsers());
        }
    }
}
