﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Entities;
using AutoMapper;

namespace BLL.Services
{
    public class AlbumService : IAlbumService
    {
        IUnitOfWork Database { get; set; }
        IMapper Mapper { get; set; }
        public AlbumService(IUnitOfWork uow)
        {
            Database = uow;
            Mapper = new MapperConfiguration(cnf => cnf.CreateMap<AlbumImage, ImageDTO>()).CreateMapper();
        }
        public async Task<OperationDetails> CreateAlbum(AlbumDTO albumDTO)
        {
            ClientAlbum clientAlbum = Database.ClientAlbums.Get(albumDTO.Id);
            if (clientAlbum == null)
            {
                clientAlbum = new ClientAlbum { AlbumImages = null, LastModified = DateTime.Now };
                Database.ClientAlbums.Create(clientAlbum);
                await Database.SaveAsync();
                return new OperationDetails(true, "Album created", $"{clientAlbum.Id}");
            }
            else
            {
                return new OperationDetails(false, "Album with this Id already exists", "");
            }
        }

        public ImageDTO GetImage(int Id)
        {
            var image = Database.AlbumeImages.Get(Id);

            if (image == null)
            {
                return null;
            }

            return new ImageDTO {Id = image.Id, Image = image.Image, AlbumId = image.ClientAlbum.Id, UploadDate = image.UploadDate };
        }

        public async Task<IEnumerable<ImageDTO>> GetImages(int id)
        {
            var album = Database.ClientAlbums.Get(id);
            if (album == null)
            {
                 await CreateAlbum(new AlbumDTO { Id = id, LastModified = DateTime.Now });
            }

            return Mapper.Map<IEnumerable<AlbumImage>, List<ImageDTO>>(Database.AlbumeImages.GetAllById(id));
        }

        public IEnumerable<ImageDTO> GetAllImages()
        {
            return Mapper.Map<IEnumerable<AlbumImage>, List<ImageDTO>>(Database.AlbumeImages.GetAll());
        }

        public async Task<OperationDetails> UpdateAlbum(AlbumDTO albumDTO)
        {
            ClientAlbum clientAlbum = Database.ClientAlbums.Get(albumDTO.Id);

            if (clientAlbum == null)
            {
                return new OperationDetails(false, "Cen not get album with given ID!", "");
            }
            clientAlbum.LastModified = DateTime.Now;
            Database.ClientAlbums.Update(clientAlbum);
            await Database.SaveAsync();

            return new OperationDetails(true, "Succesfuly updated!", "");

        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
