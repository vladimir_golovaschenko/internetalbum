﻿using System;

namespace BLL.DTO
{
    public class AlbumDTO
    {
        public int Id { get; set; }
        public DateTime LastModified { get; set; }
    }
}
