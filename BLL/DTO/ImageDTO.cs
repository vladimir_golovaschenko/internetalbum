﻿using System;

namespace BLL.DTO
{
    public class ImageDTO
    {
        public int Id { get; set; }
        public int AlbumId { get; set; }
        public byte[] Image { get; set; }
        public DateTime UploadDate { get; set; }
    }
}
