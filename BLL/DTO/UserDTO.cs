﻿using System;

namespace BLL.DTO
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int AlbumId { get; set; }
        public string Role { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}
