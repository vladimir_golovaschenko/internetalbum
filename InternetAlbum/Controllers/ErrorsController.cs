﻿using System.Web.Mvc;

namespace InternetAlbum.Controllers
{
    public class ErrorsController : Controller
    {
        // GET: Error
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        /*public ActionResult Forbidden()
        {
            Response.StatusCode = 403;
            return View();
        }*/
        public ActionResult RequestTooLarge()
        {
            Response.StatusCode = 413;
            return View();
        }
    }
}