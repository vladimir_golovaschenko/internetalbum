﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using InternetAlbum.Models;
using BLL.DTO;
using BLL.Interfaces;
using AutoMapper;
using System.Threading.Tasks;

namespace InternetAlbum.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly IAlbumService albumService;
        private readonly IImageService imageService;
        private readonly IMapper MapperImages;
        private readonly IMapper MapperUsers;

        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        public AdminController(IAlbumService albumService, IImageService imageService)
        {
            this.imageService = imageService;
            this.albumService = albumService;
            MapperImages = new MapperConfiguration(cfg => cfg.CreateMap<ImageDTO, ImageViewModel>()).CreateMapper();
            MapperUsers = new MapperConfiguration(cfg => cfg.CreateMap<UserDTO, UserViewModel>()).CreateMapper();
        }

        /// <summary>
        /// Create list of required user's data for admin page
        /// </summary>
        /// <returns>list of users</returns>
        [Authorize(Roles = "admin")]
        public ActionResult Users()
        {
            if (ModelState.IsValid)
            {
                IEnumerable<UserDTO> userDTOs = UserService.GetUsers();
                var users = MapperUsers.Map<IEnumerable<UserDTO>, List<UserViewModel>>(userDTOs);

                foreach (var user in users) {
                    user.AlbumId = UserService.GetUser(user.Id).AlbumId;
                }

                return View(users);
            }
            return View();
        }

        /// <summary>
        /// Create list of all images with their data for admin page
        /// </summary>
        /// <returns>list of images</returns>
        [Authorize(Roles = "admin")]
        public ActionResult Images()
        {
            if (ModelState.IsValid)
            {
                IEnumerable<ImageDTO> imageDTOs = albumService.GetAllImages();
                var images = MapperImages.Map<IEnumerable<ImageDTO>, List<ImageViewModel>>(imageDTOs);
                images.Reverse();

                foreach (var image in images) {
                    image.AlbumId = albumService.GetImage(image.Id).AlbumId;
                }

                return View(images);
            }
            return View();
        }

        [Authorize(Roles ="admin")]
        [HttpGet]
        public async Task<ActionResult> PartialUserImages(string id)
        {
            if (ModelState.IsValid && id != null)
            {
                var user = UserService.GetUser(id);
                ViewBag.Message = user.Name;
                IEnumerable<ImageDTO> imageDTOs = await albumService.GetImages(user.AlbumId);
                var userImages = MapperImages.Map<IEnumerable<ImageDTO>, List<ImageViewModel>>(imageDTOs);
                userImages.Reverse();
                return View(userImages);
            }
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            imageService.Delete(new ImageDTO
            {
                Id = id
            });
            return RedirectToAction("Images");
        }

        /*protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            if (filterContext.HttpContext.Response.StatusCode == 404)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Errors/404page"
                };
            }
            else if (filterContext.HttpContext.Response.StatusCode == 413)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Errors/413page"
                };
            }
            filterContext.ExceptionHandled = true;
        }*/
    }
}
