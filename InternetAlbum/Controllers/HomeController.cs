﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InternetAlbum.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /*protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            if (filterContext.HttpContext.Response.StatusCode == 404)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Errors/404page"
                };
            }
            else if (filterContext.HttpContext.Response.StatusCode == 413)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Errors/413page"
                };
            }
            filterContext.ExceptionHandled = true;
        }*/
    }
}