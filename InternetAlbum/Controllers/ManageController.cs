﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using InternetAlbum.Models;
using BLL.DTO;
using BLL.Interfaces;
using BLL.Infrastructure;
using System;
using AutoMapper;
using System.IO;
using Microsoft.AspNet.Identity;

namespace InternetAlbum.Controllers
{
    public class ManageController : Controller
    {        
        private readonly IAlbumService albumService;
        private readonly IImageService imageService;
        IMapper Mapper;
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        public ManageController(IAlbumService albumService, IImageService imageService)
        {
            this.albumService = albumService;
            this.imageService = imageService;
            Mapper = new MapperConfiguration(cfg => cfg.CreateMap<ImageDTO, ImageViewModel>()).CreateMapper();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public async Task<ActionResult> Index()
        {
            if (ModelState.IsValid)
            {
                IEnumerable<ImageDTO> imageDTOs = await albumService.GetImages(UserService.GetUser(User.Identity.GetUserId()).AlbumId);
                var images = Mapper.Map<IEnumerable<ImageDTO>, List<ImageViewModel>>(imageDTOs);
                images.Reverse();
                return View(images);
            }
            return View();
        }
        [Authorize]
        public ActionResult Upload()
        {
            return View();
        }

        [Authorize]
        public ActionResult test()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public ActionResult Delete(int id)
        {
            if (UserService.GetUser(User.Identity.GetUserId()).AlbumId == albumService.GetImage(id).AlbumId)
            {
                imageService.Delete(new ImageDTO
                {
                    Id = id
                });
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "You have not permissions to delete this image.");
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadImage"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Upload(HttpPostedFileBase uploadImage)
        {
            if (ModelState.IsValid && uploadImage != null)
            {
                string extension = Path.GetExtension(uploadImage.FileName);
                decimal fileSize = uploadImage.ContentLength;
                if (fileSize > 10485760)
                {
                    ModelState.AddModelError("Wrong file size.", "Please select file with lower size!");
                }
                else if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                {
                    byte[] imageBinary = null;

                    using (var binaryReader = new BinaryReader(uploadImage.InputStream))
                    {
                        imageBinary = binaryReader.ReadBytes(uploadImage.ContentLength);
                    }

                    ImageDTO imageDTO = new ImageDTO
                    {
                        AlbumId = UserService.GetUser(User.Identity.GetUserId()).AlbumId,
                        Image = imageBinary,
                        UploadDate = DateTime.Now
                    };

                    await imageService.Create(imageDTO);

                    OperationDetails operationDetails = await albumService.UpdateAlbum(new AlbumDTO { Id = imageDTO.AlbumId });

                    if (operationDetails.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("Album update Error", "Please try another time");
                    }
                }
                else
                {
                    ModelState.AddModelError("Wrong file extension.", "Please upload IMAGES!!!");
                }
            }

            return View("Index");
        }

        /*protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            if (filterContext.HttpContext.Response.StatusCode == 404)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Errors/404page"
                };
            }
            else if (filterContext.HttpContext.Response.StatusCode == 413)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Errors/413page"
                };
            }
            filterContext.ExceptionHandled = true;
        }*/
    }
}
