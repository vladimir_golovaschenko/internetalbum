﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Security.Claims;
using InternetAlbum.Models;
using BLL.DTO;
using BLL.Interfaces;
using BLL.Infrastructure;
using System;

namespace InternetAlbum.Controllers
{
    public class AccountController : Controller
    {
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private readonly IAlbumService albumService;

        public AccountController(IAlbumService service)
        {
            albumService = service;
        }

        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                await SetInitialDataAsync();
                UserDTO userDto = new UserDTO { Email = model.Email, Password = model.Password };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Incorrect login or password");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    return RedirectToAction("Index", "Manage");
                }
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            await SetInitialDataAsync();
            if (ModelState.IsValid)
            {
                AlbumDTO albumDTO = new AlbumDTO { LastModified = DateTime.Now };
                OperationDetails operationDetailsAlbum = await albumService.CreateAlbum(albumDTO);

                if (operationDetailsAlbum.Succeeded)
                {
                    UserDTO userDto = new UserDTO
                    {
                        Email = model.Email,
                        Password = model.Password,
                        Name = model.Name,
                        AlbumId = Convert.ToInt32(operationDetailsAlbum.Property),
                        Role = "user",
                        RegistrationDate = DateTime.Now
                    };

                    OperationDetails operationDetailsUser = await UserService.Create(userDto);

                    if (operationDetailsUser.Succeeded)
                    {
                        ClaimsIdentity claim = await UserService.Authenticate(userDto);
                        AuthenticationManager.SignOut();
                        AuthenticationManager.SignIn(new AuthenticationProperties
                        {
                            IsPersistent = true
                        }, claim);

                        return RedirectToAction("Index", "Manage");
                    }
                    else
                    {
                        ModelState.AddModelError(operationDetailsUser.Property, operationDetailsUser.Message);
                    }
                }

            }

            return View(model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task SetInitialDataAsync()
        {
            await UserService.SetInitialData(new UserDTO
            {
                Email = "admin@test.com",
                Password = "Pa$$w0rd",
                Name = "Admin Admin",
                Role = "admin"
                //AlbumId = 1,
            }, new List<string> { "user", "admin" });
        }

        /*protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            if (filterContext.HttpContext.Response.StatusCode == 404)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Errors/404page"
                };
            }
            else if (filterContext.HttpContext.Response.StatusCode == 413)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Errors/413page"
                };
            }
            filterContext.ExceptionHandled = true;
        }*/
    }
}
