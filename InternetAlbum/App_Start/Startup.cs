﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using BLL.Services;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(InternetAlbum.App_Start.Startup))]

namespace InternetAlbum.App_Start
{
    public class Startup
    {
        IServiceCreator serviceCreator = new ServiceCreator();
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<IUserService>(CreateUserService);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });

        }
        private IUserService CreateUserService()
        {
            return serviceCreator.CreateUserService("DefaultConnection");
        }
    }
}
