﻿using Ninject.Modules;
using BLL.Services;
using BLL.Interfaces;

namespace InternetAlbum.Util
{
    public class AlbumModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAlbumService>().To<AlbumService>();
        }
    }
}
