﻿using Ninject.Modules;
using BLL.Services;
using BLL.Interfaces;

namespace InternetAlbum.Util
{
    public class ImageModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IImageService>().To<ImageService>();
        }
    }
}
