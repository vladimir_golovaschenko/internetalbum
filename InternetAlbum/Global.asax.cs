﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ninject;
using BLL.Infrastructure;
using Ninject.Modules;
using InternetAlbum.Util;
using Ninject.Web.Mvc;

namespace InternetAlbum
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            NinjectModule albumModule = new AlbumModule();
            NinjectModule imageModule = new ImageModule();
            NinjectModule serviceModule = new ServiceModule("DefaultConnection");
            var kernel = new StandardKernel(albumModule, imageModule, serviceModule);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}
