﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternetAlbum.Models
{
    public class AlbumViewModel
    {
        public int Id { get; set; }
        public DateTime LastModified { get; set; }
    }
}
