﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InternetAlbum.Models
{
    public class ImageViewModel
    {
        public int Id { get; set; }
        public int AlbumId { get; set; }
        [Required]
        [FileExtensions(Extensions = ".jpeg,.jpg,.png")]
        public byte[] Image { get; set; }
        public DateTime UploadDate { get; set; }
    }
}
