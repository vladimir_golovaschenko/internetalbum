﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternetAlbum.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int AlbumId { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}
